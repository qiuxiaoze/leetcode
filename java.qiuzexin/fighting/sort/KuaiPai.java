package fighting.sort;


import java.util.Arrays;

public class KuaiPai {
    static int[] sort(int[] arr, int begin, int end){
        if(begin<end){
            int i = begin;
            int j = end;
            int x = arr[begin];
            while(i<j){
                while(i<j && arr[j]>x){
                    j--;
                }
                arr[i] = arr[j];
                while(i<j && arr[i]<=x){
                    i++;
                }
                arr[j] = arr[i];
            }
            arr[i] = x;
            sort(arr, begin, i-1);
            sort(arr, i+1, end);
        }
        return arr;
    }

    public static void main(String[] args) {
        int[] arr = new int[]{5,3,1,6,8};
        System.err.println(Arrays.toString(sort(arr, 0, arr.length-1)));
    }
}

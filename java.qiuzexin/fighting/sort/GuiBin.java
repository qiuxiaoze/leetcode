package fighting.sort;

import java.util.Arrays;

public class GuiBin {
    public static void main(String[] args) {
        int[] arr = new int[]{5,3,10,6,8,4,3,9};
        System.err.println(Arrays.toString(sort(arr, 0, arr.length-1)));
    }

    private static int[] sort(int[] arr, int begin, int end) {
        int[] temp = new int[arr.length];
        return sort(arr, begin, end, temp);
    }
    private static int[] sort(int[] arr, int begin, int end, int[] temp){
        if(begin<end){
            int mid = (begin+end)/2;
            sort(arr, begin, mid, temp);
            sort(arr, mid+1, end, temp);
            merge(arr, begin, mid, end, temp);
        }
        return arr;
    }

    private static void merge(int[] arr, int begin, int mid, int end, int[] temp) {
        int i = begin;
        int j = mid+1;
        int t = 0;
        while(i<=mid && j<=end){
            if(arr[i]<=arr[j]){
                temp[t++] = arr[i++];
            }else{
                temp[t++] = arr[j++];
            }
        }
        while(i<=mid){
            temp[t++] = arr[i++];
        }
        while(j<=end){
            temp[t++] = arr[j++];
        }
        t = 0;
        while(begin<=end){
            arr[begin++] = temp[t++];
        }
    }
}

package fighting.sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class TongPai {
    public static int[] tongPaiXu(int[] arr) {
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        for (int i : arr) {
            max = max > i ? max : i;
            min = min < i ? min : i;
        }
        int nums = (max - min) / arr.length + 1;
        ArrayList<ArrayList<Integer>> tong = new ArrayList<>(nums);
        for (int i = 0; i < nums; i++) {
            tong.add(new ArrayList<>());
        }
        for (int i : arr) {
            tong.get((i - min) / arr.length).add(i);
        }
        for (ArrayList<Integer> list : tong) {
            Collections.sort(list);
        }
        int i = 0;
        for (ArrayList<Integer> list : tong) {
            for (Integer val : list) {
                arr[i++] = val;
            }
        }
        return arr;
    }

    public static void main(String[] args) {
        int[] arr = new int[]{5,3,1,6,8};
        System.err.println(Arrays.toString(tongPaiXu(arr)));
    }
}

package fighting.SwordFingerOffer;

/**
 * 剑指 Offer 44. 数字序列中某一位的数字
 */
public class FortyFour {
    /**
     * 数字以0123456789101112131415…的格式序列化到一个字符序列中。在这个序列中，第5位（从下标0开始计数）是5，第13位是1，第19位是4，等等。
     *
     * 请写一个函数，求任意第n位对应的数字。
     *
     *
     * 示例 1：
     *
     * 输入：n = 3
     * 输出：3
     * 示例 2：
     *
     * 输入：n = 11
     * 输出：0
     *
     * 来源：力扣（LeetCode）
     * 链接：https://leetcode-cn.com/problems/shu-zi-xu-lie-zhong-mou-yi-wei-de-shu-zi-lcof
     * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
     */
    public static void main(String[] args) {
        System.err.println(findNthDigit1(1000000000));
    }
    public static int findNthDigit1(int n) {
        long wei = 1l;
        long weiCount = 9l;
        while(n>weiCount){
            n -= weiCount;
            wei++;
            weiCount = (long) ((Math.pow(10,wei-1)*9)*wei);
        }
        long numShu = (long) (Math.pow(10, wei - 1) + (n - 1) / wei);
        int index = (int) ((n-1) % wei);
        return Long.toString(numShu).charAt(Integer.valueOf(index))-'0';
    }

}

package fighting.SwordFingerOffer;


import fighting.SwordFingerOffer.entity.TreeNode;

/**
 * 剑指 Offer 55 - I. 二叉树的深度
 */
public class FiftyFive {
    /**
     *输入一棵二叉树的根节点，求该树的深度。从根节点到叶节点依次经过的节点（含根、叶节点）形成树的一条路径，最长路径的长度为树的深度。
     *
     * 例如：
     *
     * 给定二叉树 [3,9,20,null,null,15,7]，
     *
     *     3
     *    / \
     *   9  20
     *     /  \
     *    15   7
     * 返回它的最大深度3 。
     *
     * 来源：力扣（LeetCode）
     * 链接：https://leetcode-cn.com/problems/er-cha-shu-de-shen-du-lcof
     * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
     */
    public static void main(String[] args) {
        TreeNode treeNode1 = new TreeNode(3);
        TreeNode treeNode21 = new TreeNode(9);
        TreeNode treeNode22 = new TreeNode(20);
        treeNode1.left = treeNode21;
        treeNode1.right = treeNode22;
        TreeNode treeNode221 = new TreeNode(15);
        TreeNode treeNode222 = new TreeNode(7);
        treeNode22.left = treeNode221;
        treeNode22.right = treeNode222;
        System.out.println(maxDepth(treeNode1));
    }

    public static int maxDepth(TreeNode root) {
        if(root==null){
            return 0;
        }
        return Math.max(maxDepth(root.left)+1,maxDepth(root.right)+1);
    }
}

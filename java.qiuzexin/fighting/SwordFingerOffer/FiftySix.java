package fighting.SwordFingerOffer;

import java.util.Arrays;

/**
 * 剑指 Offer 56 - I. 数组中数字出现的次数
 */
public class FiftySix {
    /**
     * 一个整型数组 nums 里除两个数字之外，其他数字都出现了两次。请写程序找出这两个只出现一次的数字。要求时间复杂度是O(n)，空间复杂度是O(1)。
     *
     *
     * 示例 1：
     *
     * 输入：nums = [4,1,4,6]
     * 输出：[1,6] 或 [6,1]
     * 示例 2：
     *
     * 输入：nums = [1,2,10,4,1,4,3,3]
     * 输出：[2,10] 或 [10,2]
     *
     * 限制：
     *
     * 2 <= nums.length <= 10000
     *
     * 来源：力扣（LeetCode）
     * 链接：https://leetcode-cn.com/problems/shu-zu-zhong-shu-zi-chu-xian-de-ci-shu-lcof
     * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
     */
    public static void main(String[] args) {
        System.err.println(Arrays.toString(singleNumbers(new int[]{1,2,10,4,1,4,3,3})));
    }
    public static int[] singleNumbers(int[] nums) {
        int x=0;
        int y=0;
        int z=0;
        //得到：x^y
        for (int num : nums) {
            z^=num;
        }
        //找出某个位不同的值进行分组
        int mark = 1;
        while((z&mark)==0){
            mark<<=1;
        }
        for (int num : nums) {
            if((num&mark)==0){
                x^=num;
            }else{
                y^=num;
            }
        }
        return new int[]{x,y};
    }
}

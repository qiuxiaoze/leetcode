package fighting.SwordFingerOffer;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 剑指 Offer 38. 字符串的排列
 */
public class ThirtyEight {
    /**
     * 输入一个字符串，打印出该字符串中字符的所有排列。
     *
     *
     * 你可以以任意顺序返回这个字符串数组，但里面不能有重复元素。
     *
     *
     * 示例:
     *
     * 输入：s = "abc"
     * 输出：["abc","acb","bac","bca","cab","cba"]
     *
     * 限制：
     *
     * 1 <= s 的长度 <= 8
     *
     * 来源：力扣（LeetCode）
     * 链接：https://leetcode-cn.com/problems/zi-fu-chuan-de-pai-lie-lcof
     * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
     */
    public static void main(String[] args) {
        Arrays.stream(permutation("aab")).map(t-> {
            System.err.println(t);
            return t;
        }).collect(Collectors.toList());
    }
    static List<String> result = new LinkedList<>();
    static char[] chars;
    public static String[] permutation(String s) {
        chars = s.toCharArray();
        dfs(0);
        return result.toArray(new String[result.size()]);
    }

    private static void dfs(int x) {
        if(x==chars.length-1){
            result.add(String.valueOf(chars));
            return;
        }
        Set<Character> stringSet = new HashSet<>();
        for (int i = x; i < chars.length; i++) {
            if(stringSet.contains(chars[i])){
                //重复得剪枝
                continue;
            }
            stringSet.add(chars[i]);
            char temp = chars[i];
            chars[i] = chars[x];
            chars[x] = temp;
            dfs(x+1);
            char temp1 = chars[i];
            chars[i] = chars[x];
            chars[x] = temp1;
        }
    }

}

package fighting.SwordFingerOffer;

import java.util.Arrays;

/**
 * 剑指 Offer 60. n个骰子的点数
 */
public class Sixty {
    /**
     * 把n个骰子扔在地上，所有骰子朝上一面的点数之和为s。输入n，打印出s的所有可能的值出现的概率。
     *
     *
     * 你需要用一个浮点数数组返回答案，其中第 i 个元素代表这 n 个骰子所能掷出的点数集合中第 i 小的那个的概率。
     *
     *
     * 示例 1:
     *
     * 输入: 1
     * 输出: [0.16667,0.16667,0.16667,0.16667,0.16667,0.16667]
     * 示例2:
     *
     * 输入: 2
     * 输出: [0.02778,0.05556,0.08333,0.11111,0.13889,0.16667,0.13889,0.11111,0.08333,0.05556,0.02778]
     *
     * 来源：力扣（LeetCode）
     * 链接：https://leetcode-cn.com/problems/nge-tou-zi-de-dian-shu-lcof
     * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
     */
    public static void main(String[] args) {
        System.err.println(Arrays.toString(dicesProbability(2)));
    }
    public static double[] dicesProbability(int n) {
        double[] result = new double[6];
        Arrays.fill(result,1.0/6.0);
        for (int i = 2; i <= n; i++) {
            double[] temp = new double[5*i+1];
            for (int j = 0; j < result.length; j++) {
                for (int k = 0; k < 6; k++) {
                    temp[j+k]+=result[j]/6.0;
                }
            }
            result = temp;
        }
        return result;
    }
}

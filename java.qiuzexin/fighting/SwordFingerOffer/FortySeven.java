package fighting.SwordFingerOffer;

/**
 * 剑指 Offer 47. 礼物的最大价值
 */
public class FortySeven {
    /**
     *
     * 在一个 m*n 的棋盘的每一格都放有一个礼物，每个礼物都有一定的价值（价值大于 0）。你可以从棋盘的左上角开始拿格子里的礼物，并每次向右或者向下移动一格、直到到达棋盘的右下角。给定一个棋盘及其上面的礼物的价值，请计算你最多能拿到多少价值的礼物？
     *
     *
     * 示例 1:
     *
     * 输入:
     * [
     *  [1,3,1],
     *  [1,5,1],
     *  [4,2,1]
     * ]
     * 输出: 12
     * 解释: 路径 1→3→5→2→1 可以拿到最多价值的礼物
     *
     * 来源：力扣（LeetCode）
     * 链接：https://leetcode-cn.com/problems/li-wu-de-zui-da-jie-zhi-lcof
     * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
     */
    public static void main(String[] args) {
        System.err.println(maxValue(new int[][]{{1, 3, 1}, {1, 5, 1}, {4, 2, 1}}));
    }
    //动态规划，01背包
    public static int maxValue(int[][] grid) {
        for (int i = 1; i < grid.length; i++) {
            grid[i][0]+=grid[i-1][0];
        }
        for (int i = 1; i < grid[0].length; i++) {
            grid[0][i]+=grid[0][i-1];
        }
        for (int i = 1; i < grid.length; i++) {
            for (int j = 1; j < grid[0].length; j++) {
                grid[i][j] = Math.max(grid[i-1][j],grid[i][j-1])+grid[i][j];
            }
        }
        return grid[grid.length-1][grid[0].length-1];
    }

    //回溯超时
//    static int res = 0;
//    public static int maxValue(int[][] grid) {
//        fps(grid,0,0,0);
//        return res;
//    }
//    public static void fps(int[][] grid,int hang,int lie,int count){
//        if(hang==grid.length||lie==grid[0].length){
//            res = Math.max(res,count);
//            return;
//        }
//        for (int i = lie; i < grid[0].length; i++) {
//            count += grid[hang][i];
//            if(hang<grid.length){
//                fps(grid, hang+1, i, count);
//            }
//        }
//        res = Math.max(res,count);
//        return;
//    }
}

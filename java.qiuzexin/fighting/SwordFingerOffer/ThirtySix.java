package fighting.SwordFingerOffer;

import fighting.SwordFingerOffer.entity.Node;

import javax.swing.*;

/**
 * 剑指 Offer 36. 二叉搜索树与双向链表
 */
public class ThirtySix {
    /**
     *输入一棵二叉搜索树，将该二叉搜索树转换成一个排序的循环双向链表。要求不能创建任何新的节点，只能调整树中节点指针的指向。
     *
     * 来源：力扣（LeetCode）
     * 链接：https://leetcode-cn.com/problems/er-cha-sou-suo-shu-yu-shuang-xiang-lian-biao-lcof
     * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
     */
    public static void main(String[] args) {

    }
    static Node temp,head;
    public static Node treeToDoublyList(Node root) {
        if (root==null){
            return null;
        }
        zhongxu(root);
        temp.right = head;
        head.left = temp;
        return head;
    }
    public static void zhongxu(Node root){
        if(root==null){
            return;
        }
        treeToDoublyList(root.left);
        if(head==null){
            head = root;
        }
        if(temp==null){
            temp = root;
        }else{
            temp.right = root;
            root.left = temp;
        }
        temp = root;
        treeToDoublyList(root.right);
    }
}

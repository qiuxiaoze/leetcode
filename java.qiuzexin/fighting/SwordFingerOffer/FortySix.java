package fighting.SwordFingerOffer;

import java.util.ArrayList;
import java.util.List;

/**
 * 剑指 Offer 46. 把数字翻译成字符串
 */
public class FortySix {
    /**
     * 给定一个数字，我们按照如下规则把它翻译为字符串：0 翻译成 “a” ，1 翻译成 “b”，……，11 翻译成 “l”，……，25 翻译成 “z”。一个数字可能有多个翻译。请编程实现一个函数，用来计算一个数字有多少种不同的翻译方法。
     *
     *
     * 示例 1:
     *
     * 输入: 12258
     * 输出: 5
     * 解释: 12258有5种不同的翻译，分别是"bccfi", "bwfi", "bczi", "mcfi"和"mzi"
     *
     * 来源：力扣（LeetCode）
     * 链接：https://leetcode-cn.com/problems/ba-shu-zi-fan-yi-cheng-zi-fu-chuan-lcof
     * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
     */
    public static void main(String[] args) {
        System.err.println(translateNum(29689));
    }
    public static int translateNum(int num) {
        if(num<10){
            return 1;
        }
        int result = 1;
        int num2 = num / 10 % 10 * 10 + num % 10;
        if(9< num2 && num2<26){
            result++;
        }
        int n = 1;
        while((int)(num/Math.pow(10,++n))>0){
        }
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(result);
        for (int i = 2; i<n; i++) {
            num2 = ((int)(num/Math.pow(10,i))%10)*10+((int)(num/Math.pow(10,i-1)))%10;
            if(num2>9&&num2<26){
                list.add(list.get(i-1)+list.get(i-2));
            }else{
                list.add(list.get(i-1));
            }
        }
        return list.get(n-1);
    }
}

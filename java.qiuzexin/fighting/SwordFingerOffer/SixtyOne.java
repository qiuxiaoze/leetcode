package fighting.SwordFingerOffer;

import java.util.HashSet;
import java.util.Set;

/**
 * 剑指 Offer 61. 扑克牌中的顺子
 */
public class SixtyOne {
    /**
     * 从若干副扑克牌中随机抽 5 张牌，判断是不是一个顺子，即这5张牌是不是连续的。2～10为数字本身，A为1，J为11，Q为12，K为13，而大、小王为 0 ，可以看成任意数字。A 不能视为 14。
     *
     *
     * 示例1:
     *
     * 输入: [1,2,3,4,5]
     * 输出: True
     *
     * 示例2:
     *
     * 输入: [0,0,1,2,5]
     * 输出: True
     *
     * 来源：力扣（LeetCode）
     * 链接：https://leetcode-cn.com/problems/bu-ke-pai-zhong-de-shun-zi-lcof
     * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
     */
    public static void main(String[] args) {
        System.err.println(isStraight(new int[]{0,0,8,5,4}));
    }
    public static boolean isStraight(int[] nums) {
        int max = 0;
        int min = Integer.MAX_VALUE;
        Set<Integer> set = new HashSet<>(5);
        for (int num : nums) {
            max = Math.max(max,num);
            if(num!=0){
                min = Math.min(min,num);
                if(!set.add(num)){
                    return false;
                }
            }
        }
        return max-min<5;
    }
}

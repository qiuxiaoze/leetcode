package fighting.SwordFingerOffer;

import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

/**
 * 剑指 Offer 49. 丑数
 */
public class FortyNine {
    /**
     * 我们把只包含质因子 2、3 和 5 的数称作丑数（Ugly Number）。求按从小到大的顺序的第 n 个丑数。
     *
     *
     * 示例:
     *
     * 输入: n = 10
     * 输出: 12
     * 解释: 1, 2, 3, 4, 5, 6, 8, 9, 10, 12 是前 10 个丑数。
     *
     * 来源：力扣（LeetCode）
     * 链接：https://leetcode-cn.com/problems/chou-shu-lcof
     * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
     */
    public static void main(String[] args) {
        System.err.println(nthUglyNumber(11));
    }
    public static int nthUglyNumber(int n) {
        PriorityQueue<Long> minHeap = new PriorityQueue<>();
        Set<Long> set = new HashSet<Long>();
        minHeap.add(1L);
        Long res = 0l;
        for (int i = 0; i < n; i++) {
            res = minHeap.poll();
            if(set.add(res*2)){
                minHeap.add(res*2);
            }
            if(set.add(res*3)){
                minHeap.add(res*3);
            }
            if(set.add(res*5)){
                minHeap.add(res*5);
            }
        }
        return res.intValue();
    }
}

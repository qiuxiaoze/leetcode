package fighting.SwordFingerOffer;

import fighting.SwordFingerOffer.entity.TreeNode;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * 剑指 Offer 37. 序列化二叉树
 */
public class ThirtySeven {
    /**
     * 请实现两个函数，分别用来序列化和反序列化二叉树。
     *
     * 你需要设计一个算法来实现二叉树的序列化与反序列化。这里不限定你的序列 / 反序列化算法执行逻辑，你只需要保证一个二叉树可以被序列化为一个字符串并且将这个字符串反序列化为原始的树结构。
     *
     * 提示：输入输出格式与 LeetCode 目前使用的方式一致，详情请参阅LeetCode 序列化二叉树的格式。你并非必须采取这种方式，你也可以采用其他的方法解决这个问题。
     *
     * 来源：力扣（LeetCode）
     * 链接：https://leetcode-cn.com/problems/xu-lie-hua-er-cha-shu-lcof
     * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
     */
    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        if(root==null){
            return "[null]";
        }
        StringBuffer stringBuffer = new StringBuffer("[");
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        while(!queue.isEmpty()){
            TreeNode treeNode = queue.poll();
            if(treeNode==null){
                stringBuffer.append("null,");
            }else{
                stringBuffer.append(treeNode.val+",");
                queue.add(treeNode.left);
                queue.add(treeNode.right);
            }
        }
        stringBuffer.deleteCharAt(stringBuffer.length()-1);
        stringBuffer.append("]");
        return stringBuffer.toString();
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        if("[]".equals(data)||"[null]".equals(data)){
            return null;
        }
        String[] split = data.substring(1,data.length()-1).split(",");
        Queue<TreeNode> queue = new LinkedList<>();
        TreeNode root = new TreeNode(Integer.valueOf(split[0]));
        queue.add(root);
        int index = 1;
        while(!queue.isEmpty()){
            TreeNode temp = queue.poll();
            if(temp!=null){
                if(!"null".equals(split[index])){
                    temp.left = new TreeNode(Integer.valueOf(split[index]));
                    queue.add(temp.left);
                }
                index++;
                if(!"null".equals(split[index])){
                    temp.right = new TreeNode(Integer.valueOf(split[index]));
                    queue.add(temp.right);
                }
                index++;
            }
        }
        return root;
    }
}

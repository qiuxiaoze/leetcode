package fighting.QuestionBank;

import fighting.entity.ListNode;

import java.util.List;
import java.util.PriorityQueue;

public class Q23合并K个升序链表 {
    /**
     * 给你一个链表数组，每个链表都已经按升序排列。
     * <p>
     * 请你将所有链表合并到一个升序链表中，返回合并后的链表。
     */
    public static void main(String[] args) {
        ListNode listNode = new ListNode(7);
        ListNode listNode_1 = new ListNode(3);
        listNode.next = listNode_1;
        ListNode listNode1 = new ListNode(2);
        mergeKLists(new ListNode[]{listNode, listNode1});
    }

    public static ListNode mergeKLists(ListNode[] lists) {
        ListNode resultHead = new ListNode();
        ListNode last = resultHead;
        PriorityQueue<ListNode> priorityQueue = new PriorityQueue<ListNode>((t1, t2) -> t1.val - t2.val);
        for (ListNode node : lists) {
            if (node != null) {
                priorityQueue.add(node);
            }
        }
        while (!priorityQueue.isEmpty()) {
            ListNode poll = priorityQueue.poll();
            last.next = poll;
            last = poll;
            if (poll.next != null) {
                priorityQueue.add(poll.next);
            }
        }
        return resultHead.next;
    }
}

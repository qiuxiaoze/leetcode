package fighting.QuestionBank;

import java.util.ArrayList;
import java.util.List;

public class Q491递增子序列 {

    /**
     * 给你一个整数数组 nums ，找出并返回所有该数组中不同的递增子序列，递增子序列中 至少有两个元素 。你可以按 任意顺序 返回答案。
     * <p>
     * 数组中可能含有重复元素，如出现两个整数相等，也可以视作递增序列的一种特殊情况。
     */
    public static void main(String[] args) {
        Q491递增子序列 test = new Q491递增子序列();
        System.err.println(test.findSubsequences(new int[]{4, 4, 3, 2, 1}));
    }

    List<Integer> temp = new ArrayList<>();
    List<List<Integer>> result = new ArrayList<>();


    public List<List<Integer>> findSubsequences(int[] nums) {
        this.findSubsequences(nums, 0, Integer.MIN_VALUE);
        return result;
    }

    public void findSubsequences(int[] nums, int num, int last) {
        if (num == nums.length) {
            if (temp.size() > 1) {
                result.add(new ArrayList<>(temp));
            }
            return;
        }
        if (!(temp.size() != 0 && temp.get(temp.size() - 1) > nums[num])) {
            temp.add(nums[num]);
            this.findSubsequences(nums, num + 1, nums[num]);
            temp.remove(temp.size() - 1);
        }
        // 重复根本原因：
        // 前面选1  这次没选1
        // 前面没选1  这次选1
        // 剪枝  ——  剪掉前面选1后面没选1的 即 前面选1 这次一定得选1
        if (last == nums[num]) {
            return;
        }
        this.findSubsequences(nums, num + 1, last);
    }

}

package fighting.QuestionBank;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Q40组合总和II {

    /**
     * 给定一个候选人编号的集合 candidates 和一个目标数 target ，找出 candidates 中所有可以使数字和为 target 的组合。
     * <p>
     * candidates 中的每个数字在每个组合中只能使用 一次 。
     */
    public static void main(String[] args) {
        Q40组合总和II test = new Q40组合总和II();
        System.err.println(test.combinationSum2(new int[]{10,1,2,7,6,1,5}, 8));
    }

    public List<List<Integer>> result = new ArrayList<>();
    public List<Integer> temp = new ArrayList<>();


    public List<List<Integer>> combinationSum2(int[] candidates, int target) {
        Arrays.sort(candidates);
        combinationSum2(candidates, target, 0, 0, -1);
        return result;
    }

    public void combinationSum2(int[] candidates, int target, int sum, int index, int last) {
        if (target == sum) {
            result.add(new ArrayList<>(temp));
            return;
        }
        for (int i = index; i < candidates.length; i++) {
            if (sum > target) {
                return;
            }
            // 这次和前一个值一样，剪掉
            if (i > index && candidates[i] == candidates[i-1]){
                continue;
            }
            temp.add(candidates[i]);
            combinationSum2(candidates, target, sum + candidates[i], i + 1, i);
            temp.remove(temp.size() - 1);
        }
    }

}

package fighting.QuestionBank;

import java.util.ArrayList;
import java.util.List;

public class Q17电话号码的字母组合 {

    /**
     * 给定一个仅包含数字 2-9 的字符串，返回所有它能表示的字母组合。答案可以按 任意顺序 返回。
     * <p>
     * 给出数字到字母的映射如下（与电话按键相同）。注意 1 不对应任何字母。
     */
    public static void main(String[] args) {
        Q17电话号码的字母组合 test = new Q17电话号码的字母组合();
        System.err.println(test.letterCombinations("2"));
    }

    public static String[][] phone = new String[][]{{"a", "b", "c"}, {"d", "e", "f"}, {"g", "h", "i"}, {"j", "k", "l"},
            {"m", "n", "o"}, {"p", "q", "r", "s"}, {"t", "u", "v"}, {"w", "x", "y", "z"}};

    public List<String> letterCombinations(String digits) {
        List<String> resultList = new ArrayList<>();
        this.letterCombinations(new StringBuffer(), digits, 0, resultList);
        return resultList;
    }

    public void letterCombinations(StringBuffer result, String digits, int num, List<String> resultList) {
        if (digits.length() == num) {
            if (num != 0) {
                resultList.add(result.toString());
            }
            return;
        }
        int number = digits.charAt(num) - '2';
        for (int j = 0; j < phone[number].length; j++) {
            result.append(phone[number][j]);
            this.letterCombinations(result, digits, num + 1, resultList);
            result.deleteCharAt(result.length() - 1);
        }
    }
}

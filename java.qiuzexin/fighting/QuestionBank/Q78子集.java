package fighting.QuestionBank;

import java.util.ArrayList;
import java.util.List;

public class Q78子集 {
    /**
     * 给你一个整数数组 nums ，数组中的元素 互不相同 。返回该数组所有可能的子集（幂集）。
     * <p>
     * 解集 不能 包含重复的子集。你可以按 任意顺序 返回解集。
     */

    public static void main(String[] args) {
        Q78子集 test = new Q78子集();
        System.err.println(test.subsets(new int[]{1, 2, 3}));
    }

    public List<List<Integer>> result = new ArrayList<>();
    public List<Integer> temp = new ArrayList<>();

    public List<List<Integer>> subsets(int[] nums) {
        this.subsets(nums, 0);
        return result;
    }

    public void subsets(int[] nums, int index) {
        result.add(new ArrayList<>(temp));
        for (int i = index; i < nums.length; i++) {
            temp.add(nums[i]);
            this.subsets(nums, i + 1);
            temp.remove(temp.size() - 1);
        }
    }

}

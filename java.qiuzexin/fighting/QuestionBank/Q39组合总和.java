package fighting.QuestionBank;

import java.util.ArrayList;
import java.util.List;

public class Q39组合总和 {

    /**
     * 给你一个 无重复元素 的整数数组 candidates 和一个目标整数 target ，找出 candidates 中可以使数字和为目标数 target 的 所有 不同组合 ，并以列表形式返回。你可以按 任意顺序 返回这些组合。
     * <p>
     * candidates 中的 同一个 数字可以 无限制重复被选取 。如果至少一个数字的被选数量不同，则两种组合是不同的。
     * <p>
     * 对于给定的输入，保证和为 target 的不同组合数少于 150 个。
     */
    public static void main(String[] args) {
        Q39组合总和 test = new Q39组合总和();
        System.err.println(test.combinationSum(new int[]{2, 3, 6, 7}, 7));
    }

    public List<List<Integer>> result = new ArrayList<>();
    public List<Integer> temp = new ArrayList<>();

    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        combinationSum(candidates, target, 0, 0);
        return result;
    }

    public void combinationSum(int[] candidates, int target, int sum, int index) {
        if (target == sum) {
            result.add(new ArrayList<>(temp));
            return;
        }
        for (int i = index; i < candidates.length; i++) {
            if (sum > target) {
                return;
            }
            temp.add(candidates[i]);
            combinationSum(candidates, target, sum + candidates[i], i);
            temp.remove(temp.size() - 1);
        }
    }

}

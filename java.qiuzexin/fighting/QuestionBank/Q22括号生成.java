package fighting.QuestionBank;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Q22括号生成 {
    /**
     * 数字 n 代表生成括号的对数，请你设计一个函数，用于能够生成所有可能的并且 有效的 括号组合。
     */

    public static void main(String[] args) {
        Q22括号生成 test = new Q22括号生成();
        System.err.println(test.generateParenthesis(3));
    }

    private List<String> result = new ArrayList<>();
    private StringBuffer temp = new StringBuffer();

    private List<String> selectList = new ArrayList<>(Arrays.asList("(", ")"));

    public List<String> generateParenthesis(int n) {
        generateParenthesis(n, 0, 0, 0);
        return result;
    }

    public void generateParenthesis(int n, int index, int leftK, int rightK) {
        if (index == n * 2) {
            result.add(temp.toString());
            return;
        }
        for (int i = 0; i < selectList.size(); i++) {
            if (i == 0) {
                if (leftK >= n) {
                    continue;
                }
                leftK++;
            }
            if (i == 1) {
                if (rightK >= leftK) {
                    continue;
                }
                rightK++;
            }
            temp.append(selectList.get(i));
            generateParenthesis(n, index + 1, leftK, rightK);
            temp.deleteCharAt(temp.length() - 1);
            if (i == 0) {
                leftK--;
            }
            if (i == 1) {
                rightK--;
            }
        }

    }

}

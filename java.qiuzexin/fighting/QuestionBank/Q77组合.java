package fighting.QuestionBank;

import java.util.ArrayList;
import java.util.List;

public class Q77组合 {
    /**
     * 给定两个整数 n 和 k，返回范围 [1, n] 中所有可能的 k 个数的组合。
     */
    public static void main(String[] args) {
        Q77组合 test = new Q77组合();
        System.err.println(test.combine(4, 2));
    }

    public List<List<Integer>> result = new ArrayList<>();
    public List<Integer> temp = new ArrayList<>();


    public List<List<Integer>> combine(int n, int k) {
        this.combine(n, k, 0);
        return result;
    }

    public void combine(int n, int k, int index) {
        if (temp.size() == k) {
            result.add(new ArrayList<>(temp));
            return;
        }
        for (int i = index; i < n; i++) {
            if (temp.size() > k) {
                return;
            }
            temp.add(i + 1);
            this.combine(n, k, i + 1);
            temp.remove(temp.size() - 1);
        }
    }


}
